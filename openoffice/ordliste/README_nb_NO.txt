(English installation instructions are below.)



Norsk bokm�l myspell-ordliste for OpenOffice.org og Mozilla
===========================================================

Installering:

1. Pakk ut ordlistefilene, *.aff og *.dic, til katalogen 
   ../user/wordboook/ (I din personlige katalog hvis OpenOffice.org
   er installert p� nettverket.)
2. Legg til en linje i fila dictionary.lst som ligger i den samme katalogen:
   DICT nb NO nb_NO
3. Start OpenOffice.org og g� til:
   Verkt�y->Innstillinger->Spr�kinnstillinger->Skrivehjelpemidler
4. Velg �Rediger� og velg spr�k fra nedtrekksmenyen. Kontroller at
   �MySpell SpellChecker� er krysset av.
5. Stavekontrollen skal n� fungere for tekst som er formatert som 
   norsk bokm�l.

Du kan ogs� bruke et grafisk installasjonsprogram:
Windows: http://www.ooodocs.org/dictinstall
Linux/Unix: http://ooodi.sourceforge.net



Norwegian Bokm�l myspell-dictionary for OpenOffice.org and Mozilla
==================================================================

How to Install:

1. Unzip the dictionary files, *.aff and *.dic, into your
   OpenOffice.org  ../user/wordbook/  directory (under your personal install if network install).
2. Edit the dictionary.lst file that is in that same directory
   adding the line: DICT nb NO nb_NO
3. Start up OpenOffice.org. and go to:
   Tools->Options->LanguageSettings->WritingAids
4. Chose "Edit" and use the pull-down menu to select your locale,
   "Norway". Make sure to check the MySpell SpellChecker! 
5. The spellchecker should now be active for text marked as norwegian bokm�l.

OR use a gui-installer:
Windows: http://www.ooodocs.org/dictinstall
Linux/Unix: http://ooodi.sourceforge.net






The MySpell-files are converted from ispell-dictionary that are maintained by Rune Kleveland <runekl@math.uio.no> at http://folk.uio.no/runekl/dictionary.html under a GPL + Mozilla/OpenOffice-licence:

"I hereby give the openOffice project and the Mozilla project
premission to use the Norwegian dictionaries copyrighted by me in
their project.

Rune Kleveland"

Dictionary converted from myspell with perl-script written by Toralf Lund <toralf@kscanners.com>.

====
Marius Kotsbak <marius@kotsbak.com>
