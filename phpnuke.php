<?php

/**************************************************************************/
/* PHP-NUKE: The Future of the Web                                        */
/* ===============================                                        */
/*                                                                        */
/* This is the language module with "all" the system messages sorted by   */
/* modules.                                                               */
/*                                                                        */
/* If you made a translation, please sent to me (fbc@mandrakesoft.com)    */
/* the translated file. Please keep the original text order by modules,   */
/* and just one message per line, also double check your translation!     */
/*                                                                        */
/* You need to change the second quoted phrase, not the capital one!      */
/*                                                                        */
/* If you need to use double quotes (") remember to add a backslash (\),  */
/* so your entry will look like: This is \"double quoted\" text.          */
/* And, if you use HTML code, please double check it.                     */
/**************************************************************************/
/*                                                                        */
/* Language            : Norwegian                                        */
/* PHP-Nuke version    : v5.0.RC1                                         */
/* Translation by      : Christopher Thorjussen (joffer@online.no)        */
/*                       http://www.nukemodules.com                       */
/* Last changed        : 2001-06-15                                       */
/*                                                                        */
/* NOTE: If you find any misspellings or bugs, please send your sentences */
/*       to me at joffer@online.no                                        */
/*                                                                        */
/*       Please note that the translation for the new download script     */
/*       was done real quick, I also think the script is no good at this  */
/*       time (it looks great, but just don't work well in RC1), which    */
/*       is one reason I didn't put too much soul in that part of the     */
/*       translation (the download module part that is).                  */
/*                                                                        */
/*       Use this file whatever way you want ;-)                          */
/*                                                                        */
/* Download this file (or newer version if available), other language     */
/* files, PHP-Nuke 5 Modules & Themes etc from http://www.nukemodules.com */
/*                                                                        */
/**************************************************************************/

/*****************************************************/
/* Charset for META Tags                             */
/*****************************************************/

define("_CHARSET","ISO-8859-1");

/*****************************************************/
/* Common texts                                      */
/*****************************************************/

define("_SEND","Send");
define("_URL","URL");
define("_PRINTER","Printervennlig side");
define("_FRIEND","Send denne artikkelen til en venn");
define("_SEARCH","S�k");
define("_LOGIN"," Login ");
define("_WRITES","skriver");
define("_POSTEDON","Posted on");
define("_NICKNAME","Brukernavn");
define("_PASSWORD","Passord");
define("_WELCOMETO","Velkommen til");
define("_EMAIL","Epost");
define("_REALNAME","Fullt navn");
define("_FUNCTIONS","Funksjoner");
define("_EDIT","Redigere");
define("_DELETE","Slett");
define("_PREVIOUS","Forrige side");
define("_NEXT","Neste side");
define("_YOURNAME","Ditt navn");
define("_SORTASC","Sorter stigende");
define("_SORTDESC","Sorter synkende");
define("_POSTEDBY","Postet av");
define("_CANCEL","Kanseler");
define("_YES","Ja");
define("_NO","Nei");
define("_ALLTOPICS","Alle emnene");
define("_READS","ganger lest");
define("_CATEGORY","Kategori");
define("_OTHEROPTIONS","Moduler");
define("_GOBACK","[ <a href=\"javascript:history.go(-1)\">G� tilbake</a> ]");
define("_GOBACK2","G� tilbake");
define("_OPTIONAL","(ikke n�dvendig)");
define("_REQUIRED","(m� fylles ut)");
define("_SAVECHANGES","Lagre endringer");
define("_RE","Re");
define("_OK","Ok!");
define("_SAVE","Lagre");
define("_ID","ID");

/*****************************************************/
/* Articles texts                                    */
/*****************************************************/

define("_RELATED","Relaterte linker");
define("_MOREABOUT","Mer om");
define("_NEWSBY","Nyheter av");
define("_MOSTREAD","Mest leste artikkel om");
define("_LASTNEWS","Siste nyhet(er) om");
define("_READMORE","Les mer...");
define("_BYTESMORE","bytes mer");
define("_COMMENTSQ","kommentarer?");
define("_COMMENT","kommentar");
define("_COMMENTS","kommentarer");
define("_PASTARTICLES","Tidligere artikler");
define("_OLDERARTICLES","Eldre artikler");

/*****************************************************/
/* Comments texts (Articles and Polls)               */
/*****************************************************/

define("_CONFIGURE","Kommentaroppsett");
define("_LOGINCREATE","Login/Opprett en konto");
define("_THRESHOLD","Laveste score");
define("_NOCOMMENTS","Ingen kommentarer");
define("_NESTED","Innrykket");
define("_FLAT","I rekkef�lge");
define("_THREAD","Tr�det");
define("_OLDEST","Eldste f�rst");
define("_NEWEST","Nyeste f�rst");
define("_HIGHEST","H�yeste poeng f�rst");
define("_COMMENTSWARNING","Alle kommentarer/innlegg er forfatterens ansvar. Vi tar ikke ansvar for innholdet.");
define("_SCORE","Score:");
define("_BY","av");
define("_ON","p�");
define("_USERINFO","Brukerinfo");
define("_READREST","Les resten av denne kommentaren...");
define("_REPLY","Svar p� dette");
define("_REPLYMAIN","Post kommentar");
define("_NOSUBJECT","Ingen emne");
define("_NOANONCOMMENTS","Anonyme brukere kan ikkee poste kommentarer. Vennligst <a href=\"user.php\">registrer deg</a>");
define("_PARENT","Forrige");
define("_ROOT","Root");
define("_LOGOUT","Logg ut");
define("_UCOMMENT","Kommentar");
define("_ALLOWEDHTML","Tillatt HTML:");
define("_POSTANON","Anonymt Innlegg");
define("_EXTRANS","Extrans (html tagger i tekst)");
define("_HTMLFORMATED","HTML Formatert");
define("_PLAINTEXT","Standard ASCII tekst");
define("_ONN","on...");
define("_SUBJECT","Emne");
define("_DUPLICATE","Duplikat. Sendte du inn to ganger?");
define("_COMMENTSBACK","Tilbake til kommentarene");
define("_COMMENTREPLY","Kommenter innlegg");
define("_COMREPLYPRE","Forh�ndsvis kommentar");
define("_SURVEYCOM","Kommenter Avstemning");
define("_SURVEYCOMPRE","Forh�ndsvis avstemningskommentar");
define("_NOTRIGHT","Noe er galt ved posting av variabel til denne funksjonen. Denne meldingen er her bare for � forhindre mer rot.");
define("_DIRECTCOM","Direkte kommentar til avstemningnen");
define("_SENDAMSG","Send en melding");

/*****************************************************/
/* Send to Friend and Recommend us texts             */
/*****************************************************/

define("_YOUSENDSTORY","Du vil sende artikkelen");
define("_TOAFRIEND","til en spesifikk venn:");
define("_FYOURNAME","Ditt navn:");
define("_FYOUREMAIL","Din epost:");
define("_FFRIENDNAME","Din venns navn:");
define("_FFRIENDEMAIL","Din venns epost:");
define("_INTERESTING","Interessant artikkel p�");
define("_HELLO","Hallo");
define("_YOURFRIEND","Din venn");
define("_CONSIDERED","fant den f�lgende artikkelen interessant, og �nsket � sende den til deg.");
define("_FDATE","Dato:");
define("_FTOPIC","Emne:");
define("_YOUCANREAD","Du kan lese interessante artikler p�");
define("_FSTORY","Artikkel");
define("_HASSENT","Har blitt sendt til");
define("_THANKS","Takk!");
define("_RECOMMEND","Anbefal denne portalen til en venn");
define("_INTSITE","Interesant portal:");
define("_OURSITE","fant v�r side");
define("_INTSENT","interesant og �nsket � sende den til deg.");
define("_FSITENAME","Portal navn:");
define("_FSITEURL","Portalens URL:");
define("_FREFERENCE","Referansen til v�r portal har blitt sendt til");
define("_THANKSREC","Takk for at du annbefalte oss!");
define("_PDATE","Dato:");
define("_PTOPIC","Emne:");
define("_COMESFROM","Denne artikkelen kommer fra");
define("_THEURL","Adressen (URL) til denne artikkelen er:");

/*****************************************************/
/* Mainfile texts                                    */
/*****************************************************/

define("_MPROBLEM","Et problem oppstod!");
define("_CATEGORIES","Kategorier");
define("_WAITINGCONT","Innsendt stoff");
define("_SUBMISSIONS","Artikler");
define("_WREVIEWS","Anmeldelser");
define("_WLINKS","Linker");
define("_EPHEMERIDS","Begivenheter");
define("_ONEDAY","En dag som i dag...");
define("_ASREGISTERED","Er du ikke registrert enda? Du kan <a href=\"user.php\">registrere deg gratis her</a>.");
define("_MENUFOR","Meny for");
define("_NOBIGSTORY","Det er ingen popul�r artikkel i dag, s� langt..");
define("_BIGSTORY","Dagens mest leste artikkel er:");
define("_TODAYBIG","Dagens artikkel");

/*****************************************************/
/* Polls System texts                                */
/*****************************************************/

define("_SURVEY","Avstemning");
define("_PASTSURVEYS","Tidligere Avstemninger");
define("_POLLS","Avstemninger");
define("_PCOMMENTS","Kommentarer:");
define("_RESULTS","Resultater");
define("_LVOTES","stemmer");
define("_TOTALVOTES","Antall stemmer:");
define("_ONEPERDAY","Du kan kun avgi stemme �n gang per dag");
define("_VOTING","Stemmeboks");
define("_OTHERPOLLS","Andre Avstemninger");
define("_CURRENTSURVEY","Aktiv avstemning");
define("_CURRENTPOLLRESULTS","Avstemningsresultat");

/*****************************************************/
/* Headlines texts                                   */
/*****************************************************/

define("_HREADMORE","les mer..");

/*****************************************************/
/* Who's Online                                      */
/*****************************************************/

define("_CURRENTLY","Det er ");
define("_GUESTS","gjest(er) og");
define("_MEMBERS","medlem(mer) online akkurat n�.");
define("_WHOSONLINE","Hvem er Online");
define("_YOUARELOGGED","Du er logget inn som");
define("_YOUHAVE","Du har");
define("_PRIVATEMSG","privat(e) melding(er).");
define("_YOUAREANON","Anonym bruker. Du kan registrere deg gratis <a href=\"user.php\">her</a>");

/*****************************************************/
/* Members List texts                                */
/*****************************************************/

define("_MEMBERSLIST","Medlemsliste");
define("_GREETINGS","Vi �nsker v�r siste registrerte bruker velkommen:");
define("_SORTBY","Sorter etter:");
define("_MNICKNAME","nick");
define("_MREALNAME","virkelig navn");
define("_MEMAIL","epost");
define("_MURL","url");
define("_ONLINEREG","Antall registrerte brukere s�langt:");
define("_WEHAVE","Vi har");
define("_MREGISTERED","registrerte brukere s�langt. Det er");
define("_MREGONLINE","registrerte bruker(e) online akkurat n�.");
define("_REGSOFAR","registrerte brukere s�langt.");
define("_USERSFOUND","bruker(e) funnet p�");
define("_MPAGES","sider");
define("_USERSSHOWN","brukere vist");
define("_NOMEMBERS","Ingen brukere funnet p�");

/*****************************************************/
/* Reviews texts                                     */
/*****************************************************/

define("_WRITEREVIEW","Skriv en anmeldelse");
define("_WRITEREVIEWFOR","Skriv en anmeldelse for");
define("_ENTERINFO","Vennligst skriv informasjon i hennhold til spesifikasjonene");
define("_PRODUCTTITLE","Produkt tittel");
define("_NAMEPRODUCT","Navn p� produkt.");
define("_REVIEW","Anmeldelse");
define("_CHECKREVIEW","Ditt anmeldelse. Pass p� ordspr�ket! Skriv minst 100 ord, OK? Du kan ogs� bruke HTML tagger om du vet hvordan du bruker dem.");
define("_FULLNAMEREQ","Ditt fulle navn. (M� fylles ut).");
define("_REMAIL","Din epost");
define("_REMAILREQ","Din epostadresse. (M� fylles ut).");
define("_SELECTSCORE","Produktets karakter");
define("_RELATEDLINK","Relatert link");
define("_PRODUCTSITE","Offisiell hjemmeside. S�rg for at URL'en starter med \"http://\"");
define("_LINKTITLE","Link tittel");
define("_LINKTITLEREQ","Kreves dersom du har en relatert link.");
define("_RIMAGEFILE","Filnavn bilde");
define("_RIMAGEFILEREQ","Navn p� cover bilder, lokalisert i \"images/reviews/\". Ikke n�dvendig.");
define("_CHECKINFO","Vennligst s�rg for at informasjonen er 100% gyldig, og det er brukt bra grammatikk. For eksempel s� skriver du ikke all tekst med STORE BOKSTAVER, da det bli bli forkastet.");
define("_INVALIDTITLE","Feil i TITTEL. Kan ikke v�re blank");
define("_INVALIDSCORE","Feil KARAKTERVERDI.. M� v�re mellom 1 og 10");
define("_INVALIDTEXT","Feil i TEKSTEN. Kan ikke v�re blank");
define("_INVALIDHITS","Hits m� v�re av positiv verdi");
define("_CHECKNAME","Du m� skrive b�de navn og epostadresse");
define("_INVALIDEMAIL","Feil i epostadresse (f.eks: you@hotmail.com)");
define("_INVALIDLINK","Du m� skrive B�DE link tittel og link adresse, eller la begge v�re blanke");
define("_ADDED","Lagt til:");
define("_REVIEWER","Anmelder:");
define("_REVIEWID","Anmeldelses ID");
define("_HITS","Hits");
define("_LOOKSRIGHT","Ser dette rett ut?");
define("_RMODIFIED","redigert");
define("_RADDED","lagt til");
define("_NOTE","Note:");
define("_ADMINLOGGED","Du er logget inn som Admin... anmeldelsen vil bli postet direkte");
define("_RTHANKS","Takk for at du sendte inn denne anmeldelsen");
define("_MODIFICATION","modifisering");
define("_ISAVAILABLE","Den er n� tilgjengelig i anmeldelsesdatabasen.");
define("_EDITORWILLLOOK","Ansvarlige vil se p� din anmeldelse. Den er nok klar snart!");
define("_RBACK","Tilbake til anmeldelsesoversikten");
define("_RWELCOME","Velkommen til portalens anmeldelser");
define("_10MOSTPOP","10 mest popul�re anmeldelser");
define("_10MOSTREC","10 siste anmeldelser");
define("_THEREARE","Det er");
define("_REVIEWSINDB","anmeldelser i databasen");
define("_REVIEWS","Anmeldelser");
define("_REVIEWSLETTER","Anmeldelser for bokstaven");
define("_NOREVIEWS","Det eksisterer ingen anmeldelser for bokstaven");
define("_TOTALREVIEWS","Antall anmeldelser funnet.");
define("_RETURN2MAIN","Tilbake til hovedmenyen");
define("_REVIEWCOMMENT","Kommenter anmeldelse:");
define("_YOURNICK","Ditt nickname:");
define("_RCREATEACCOUNT","<a href=user.php>Lag en konto</a>");
define("_YOURCOMMENT","Din kommentar:");
define("_MYSCORE","Min score:");
define("_ADMIN","Admin:");
define("_REVIEWMOD","Modifiser anmeldelse");
define("_RDATE","Dato:");
define("_RTITLE","Tittel:");
define("_RTEXT","Tekst:");
define("_REVEMAIL","Epost:");
define("_RLINK","Link:");
define("_RLINKTITLE","Link Tittel:");
define("_COVERIMAGE","Cover bilde:");
define("_PREMODS","Forh�ndsvis modifikasjoner");

/*****************************************************/
/* Search texts                                      */
/*****************************************************/

define("_SEARCHUSERS","S�k i brukerdatabasen");
define("_SEARCHSECTIONS","S�k i seksjonsdokumentene");
define("_SEARCHREVIEWS","S�k i anmeldelsene");
define("_SEARCHIN","S�k i");
define("_ARTICLES","Articles");
define("_ALLAUTHORS","Alle forfattere");
define("_ALL","Alle");
define("_WEEK","uke");
define("_WEEKS","uker");
define("_MONTH","m�ned");
define("_MONTHS","m�neder");
define("_SEARCHON","S�k i:");
define("_SSTORIES","Artikler");
define("_SSECTIONS","Avsnitt");
define("_SUSERS","Brukere");
define("_NOMATCHES","S�ket gav ingen resultater");
define("_PREVMATCHES","forrige treff");
define("_NEXTMATCHES","neste treff");
define("_INSECTION","i seksjon");
define("_NONAME","Ingen navn inntastet");
define("_SCOMMENTS","Kommentarer");
define("_SEARCHRESULTS","S�keresultat");
define("_CONTRIBUTEDBY","Innsendt av");
define("_UCOMMENTS","Kommentarer");
define("_MATCHTITLE","Treff p� tittel");
define("_MATCHTEXT","Treff p� artikkelens tekst");
define("_MATCHBOTH","Treff p� tittel og artikkelens tekst");
define("_SREPLY","Svar");
define("_SREPLIES","Svar");
define("_ATTACHART","Vedlagt artikkel");
define("_PAGES","Sider");
define("_REVIEWSCORE","Poengsum for denne anmeldelsen");

/*****************************************************/
/* Special Sections texts                            */
/*****************************************************/

define("_SECWELCOME","Velkommen til seksjonene hos");
define("_YOUCANFIND","Her vil du finne interessante dokumenter som du ikke vil finne under hovedmenyen.");
define("_THISISSEC","Dette er avsnittet om");
define("_FOLLOWINGART","F�lgende artikler er publisert under dette avsnittet.");
define("_SECRETURN","Tilbake til oversikten");
define("_TOTALWORDS","antall ord i denne teksten");
define("_BACKTO","Tilbake til");
define("_SECINDEX","Avsnittsoversikten");
define("_PAGE","Side");
define("_PAGEBREAK","Dersom du vil ha flere sider kan du skrive <b>&lt;!--pagebreak--&gt;</b> hvor du vil kutte.");
define("_DONTSELECT","NB: Ikke velg en seksjon til � lagre teksten og publiser senere.");

/*****************************************************/
/* Stats texts                                       */
/*****************************************************/

define("_WERECEIVED","Vi har hatt");
define("_PAGESVIEWS","sidevisninger siden");
define("_BROWSERS","Nettlesere");
define("_OPERATINGSYS","Operativsystem");
define("_UNKNOWN","Ukjent");
define("_OTHER","Andre / Ukjente");
define("_MISCSTATS","Diverse statistikk");
define("_REGUSERS","Registrerte brukere:");
define("_ACTIVEAUTHORS","Antall ansvarlige:");
define("_STORIESPUBLISHED","Artikkler postet:");
define("_SACTIVETOPICS","Aktive emner:");
define("_COMMENTSPOSTED","Kommentarer postet:");
define("_SSPECIALSECT","Seksjoner:");
define("_ARTICLESSEC","Seksjonsdokumenter:");
define("_LINKSINLINKS","Linker databasen:");
define("_LINKSCAT","Lenkekategorier:");
define("_NEWSWAITING","Artikler som venter:");
define("_NUKEVERSION","PHP-Nuke versjon:");
define("_SEARCHENGINES","S�kemotorer");
define("_BOTS","Roboter/Spiders");
define("_STATS","Statistikk");

/*****************************************************/
/* Submit texts                                      */
/*****************************************************/

define("_SUBMITNEWS","Send inn nyheter");
define("_SUBMITADVICE","Vennligst skriv din artikkel/nyhet ved � fylle ut f�lgende skjema og dobbeltsjekk teksten for feil.<br>V�r oppmerksom p� at ikke alle innsendte artikler/nyheter blir postet.<br>Din innsending vil bli sjekket og kanskje redigert av ansvarlige.");
define("_SUBTITLE","Tittel");
define("_BEDESCRIPTIVE","V�r klar og enkel i beskrivelen");
define("_BADTITLES","D�rlig tittel='Sjekk dette!' or 'En artikkel'");
define("_TOPIC","Emne");
define("_ARTICLETEXT","Artikkel tekst:");
define("_HTMLISFINE","HTML er ok, men dobbeltsjekk URL'ene og HTML taggene!");
define("_AREYOUSURE","Er du sikker p� at du har inkludert en URL? Har du sjekket for skrivefeil?");
define("_SUBPREVIEW","Du m� forh�ndsvise f�r du kan publisere");
define("_SELECTTOPIC","Velg emne");
define("_NEWSUBPREVIEW","Forh�ndsvis innsendt post");
define("_STORYLOOK","Din nyhet/artikkel vil omtrent se slik ut:");
define("_CHECKSTORY","Vennligst sjekk tekst, linker etc f�r du poster!");
define("_THANKSSUB","Takk for ditt bidrag!");
define("_SUBSENT","Din artikkel/nyhet er mottatt...");
define("_SUBTEXT","Vi vil sjekke ditt bidrag s� fort som mulig. Er det av interesse og relevant, s� vil vi poste det s� fort som mulig.");
define("_WEHAVESUB","Vi har");
define("_WAITING","bidrag som venter p� � bli publisert.");
define("_PREVIEW","Forh�ndsvis");

/*****************************************************/
/* Topics Page texts                                 */
/*****************************************************/

define("_ACTIVETOPICS","Aktive emner");
define("_CLICK2LIST","Klikk for en liste over alle artikler i dette emnet");

/*****************************************************/
/* TOP Page texts                                    */
/*****************************************************/

define("_TOPWELCOME","Her har du de mest popul�re sidene hos");
define("_READSTORIES","mest leste innleggene");
define("_COMMENTEDSTORIES","innleggene med flest kommentarer");
define("_ACTIVECAT","mest aktive kategorier");
define("_READSECTION","mest leste seksjonsdokumenter");
define("_NEWSSUBMITTERS","mest aktive bidragsyterne til nyhetene");
define("_NEWSSENT","sendt nyheter");
define("_VOTEDPOLLS","mest popul�re unders�kelsene");
define("_MOSTACTIVEAUTHORS","mest aktive admin'ene");
define("_NEWSPUBLISHED","nyheter publisert");
define("_READREVIEWS","mest leste anmeldelser");
define("_DOWNLOADEDFILES","mest popul�re downloads");
define("_DOWNLOADS","Downloads");

/*****************************************************/
/* User's System texts                               */
/*****************************************************/

define("_ERRORINVEMAIL","FEIL: Feil epost");
define("_ERROREMAILSPACES","FEIL: Epostadresser kan ikke inneholde mellomrom");
define("_ERRORINVNICK","FEIL: Ugyldig brukernavn");
define("_NICK2LONG","Nick er for langt. Maks 25 bokstaver");
define("_NAMERESERVED","FEIL: Dette navnet er reservert");
define("_NICKNOSPACES","FEIL: Det kan ikke v�re mellomrom i nicks");
define("_NICKTAKEN","FEIL: Nicket er allerede tatt");
define("_EMAILREGISTERED","FEIL: Epostadressen er allerede registrert");
define("_UUSERNAME","Brukernanv");
define("_FINISH","Send registrering");
define("_YOUUSEDEMAIL","Du eller en annen har brukt din epostadresse");
define("_TOREGISTER","til � registrere en konto hos");
define("_FOLLOWINGMEM","F�lgende medlemsinformasjon er registrert:");
define("_UNICKNAME","-Nick:");
define("_UPASSWORD","-Passord:");
define("_USERPASS4","Brukerpassord for");
define("_YOURPASSIS","Ditt passord er:");
define("_2CHANGEINFO","for � forandre din info");
define("_YOUAREREGISTERED","Du er n� registrert. Du vil motta ditt passord p� den epostadressen du oppgav.");
define("_THISISYOURPAGE","Dette er din personlige side. Det vil sikkert v�re i din interesse � gj�re noen endringer. Dersom du vil kaste bort litt tid, s� er dette rett plass.");
define("_AVATAR","Avatar");
define("_WEBSITE","Hjemmeside");
define("_ICQ","ICQ nummer");
define("_AIM","AIM nummer");
define("_YIM","YIM nummer");
define("_MSNM","MSN nummer");
define("_LOCATION","Hvor fra");
define("_OCCUPATION","Yrke");
define("_INTERESTS","Interesser");
define("_SIGNATURE","Signatur");
define("_MYHOMEPAGE","Min hjemmeside:");
define("_MYEMAIL","Min epost:");
define("_EXTRAINFO","Ekstra info");
define("_NOINFOFOR","Det er ikke noe tilgjengelig informasjon for");
define("_LAST10COMMENTS","Siste 10 kommentarer fra");
define("_LAST10SUBMISSIONS","Siste 10 artikler/nyheter innsendt av");
define("_LOGININCOR","Feil login! Vennligst pr�v igjen...");
define("_USERLOGIN","Bruker login");
define("_USERREGLOGIN","Login &amp; Brukerregistrering");
define("_REGNEWUSER","Ny brukerregistrering");
define("_LIST","List");
define("_NEWUSER","Ny bruker");
define("_PASSWILLSEND","(Passordet vil bli sendt til epostadressen du oppgav)");
define("_COOKIEWARNING","NB: Kontoens innstillinger er cookie basert.");
define("_ASREGUSER","Som registret bruker kan du:");
define("_ASREG1","Skrive kommentarer med ditt navn");
define("_ASREG2","Sende inn nyheter i ditt navn");
define("_ASREG3","Ha din egen personlige boks i hovedmenyen");
define("_ASREG4","Velg hvor mange nyheter du vil ha i hovedmenyen");
define("_ASREG5","Tilpasse kommentarene");
define("_ASREG6","Velg mellom forskjellige utseender (themes)");
define("_ASREG7","andre kule saker...");
define("_REGISTERNOW","Registrer deg n�! Det er gratis!");
define("_WEDONTGIVE","Vi hverken selger eller gir bort din personlige informasjon.");
define("_ALLOWEMAILVIEW","La andre brukere se din epost adresse");
define("_OPTION","Mulighet");
define("_PASSWORDLOST","Glemt passordet ditt?");
define("_NOPROBLEM","Ikke noe problem. Bare skriv inn ditt nick og klikk p� send knappen. Vi vil da automatisk sende deg en epost med din bekreftelseskode. Deretter skriver du nicket ditt p� nytt og ogs� bekreftelsekoden du mottok, og vi vil sende deg nytt passord til epostadressen din.");
define("_CONFIRMATIONCODE","Bekreftelseskode");
define("_SENDPASSWORD","Send passord");
define("_YOUARELOGGEDOUT","Du er n� logget ut av systemet!");
define("_EMAILALREADYEXIST","FEIL: Epostadressen er allerede registrert");
define("_SORRYNOUSERINFO","Beklager, ingen sammenheng med brukerinfo ble funnet");
define("_USERACCOUNT","Brukerkonto");
define("_AT","ved");
define("_HASTHISEMAIL","har denne epostadressen tilknyttet seg.");
define("_AWEBUSERFROM","En webbruker fra");
define("_HASREQUESTED","har bedt om � f� tilsendt passord.");
define("_YOURNEWPASSWORD","Ditt nye passord er:");
define("_YOUCANCHANGE","Du kan endre det etter at du har logget deg inn p�");
define("_IFYOUDIDNOTASK","Ikke v�r redd dersom du ikke har bedt om dette. Det er du som ser denne meldingen, ikke 'dem'. Dersom dette var en feil, s� logg inn med det nye passordet.");
define("_UPDATEFAILED","Could not update user entry. Please, Contact the Administrator.");
define("_PASSWORD4","Passord for");
define("_MAILED","Postet.");
define("_CODEREQUESTED","har forespurt om en bekreftelseskode for � endre passordet.");
define("_YOURCODEIS","Din bekreftelseskode er:");
define("_WITHTHISCODE","Med denne koden kan du n� tildeles et nytt passord til");
define("_IFYOUDIDNOTASK2","Hvis du ikke ba om denne eposten, er det ingen fare. Bare slett den.");
define("_CODEFOR","Bekreftelseskode for");
define("_USERPASSWORD4","Brukerpassord for");
define("_UREALNAME","Fullt Navn");
define("_UREALEMAIL","Ekte epostadresse");
define("_EMAILNOTPUBLIC","(Denne epostadressen vil ikke v�re offenlig, men er n�dvendig, da den vil bli brukt til � sende deg ditt passord i tilfelle du glemmer det.)");
define("_UFAKEMAIL","Falsk epostadresse");
define("_EMAILPUBLIC","(Denne Eepostadressen er offentlig. Skriv hva du vil, SPAM sikker)");
define("_YOURHOMEPAGE","Din hjemmeside");
define("_YOURAVATAR","Din Avatar");
define("_YICQ","ICQ");
define("_YAIM","AIM");
define("_YYIM","YIM");
define("_YMSNM","MSNM");
define("_YLOCATION","Hvor fra");
define("_YOCCUPATION","Yrke");
define("_YINTERESTS","Dine interesser");
define("_255CHARMAX","(Max 255 bokstaver. Skriv din signatur med HTML koding)");
define("_CANKNOWABOUT","(255 tegn maks. Skriv hva du vil andre skal vite om deg)");
define("_TYPENEWPASSWORD","(tast inn det nye passordet to ganger for � endre det)");
define("_SOMETHINGWRONG","Noe gikk galt... skikkelig kjedelig, ikke sant?");
define("_PASSDIFFERENT","Begge passordene er forskjellige. De m� v�re like.");
define("_YOURPASSMUSTBE","Beklager, ditt passord m� v�re minst");
define("_CHARLONG","tegn langt");
define("_AVAILABLEAVATARS","Avatar's liste");
define("_NEWSINHOME","Antall poster p� forsiden");
define("_MAX127","(max. 127):");
define("_ACTIVATEPERSONAL","Aktiver personlig meny");
define("_CHECKTHISOPTION","(Kryss av for dette valg, og f�lgende tekst vil komme fram i hovedmenyen)");
define("_YOUCANUSEHTML","(Du kan bruke HTML kode for � legge til linker, for eksempel)");
define("_SELECTTHEME","Velg et Utseende");
define("_THEMETEXT1","Dette valget vil endre utseende p� hele nettstedet.");
define("_THEMETEXT2","Disse endringene p�virker bare deg.");
define("_THEMETEXT3","Hver bruker kan se nettstedet med forskjellig utseende.");
define("_DISPLAYMODE","Visningsmodus");
define("_SORTORDER","Sorteringsrekkef�lge");
define("_COMMENTSWILLIGNORED","Kommentarer som f�r lavere poengscore enn denne innstillingen, vil bli ignorert.");
define("_UNCUT","R�tt og brutalt");
define("_EVERYTHING","Nesten alt");
define("_FILTERMOSTANON","Filtrer flest anonyme *sjekk*");
define("_USCORE","Poeng");
define("_SCORENOTE","Anonyme innslag starter p� 0, registrerte brukeres innslag starter p� 1. Moderatorer legger til og trekker fra poeng.");
define("_NOSCORES","Ikke vis poeng");
define("_HIDDESCORES","(Gjem poeng: De vil fortsatt gjelde, du vil bare ikke se dem.)");
define("_MAXCOMMENT","Maks kommentarlengde");
define("_TRUNCATES","(Sammentrekker lange kommentarer, og legger til en \"Les mer...\" link. Sett den veldig h�yt for � koble ut funksjonen.)");
define("_BYTESNOTE","bytes (1024 bytes = 1K)");
define("_USENDPRIVATEMSG","Send en privat melding til");
define("_THEMESELECTION","Themevalg");
define("_COMMENTSCONFIG","Kommentarkonfigurasjon");
define("_HOMECONFIG","Portalkonfigurasjon *sjekk*");
define("_PERSONALINFO","Personlig informasjon");
define("_USERSTATUS","Brukerstatus");
define("_ONLINE","Online");
define("_OFFLINE","Offline");
define("_CHANGEYOURINFO","Forandre din brukerinfo");
define("_CONFIGCOMMENTS","Kommentaroppsett");
define("_CHANGEHOME","Forandre portalen");
define("_LOGOUTEXIT","Logout/Exit");
define("_SELECTTHETHEME","Velg theme");

/*****************************************************/
/* FAQ texts                                         */
/*****************************************************/

define("_FAQ2","FAQ (Frequently Asked Questions)");
define("_BACKTOTOP","Til topps");
define("_BACKTOFAQINDEX","Tilbake til FAQ indeks");

/*****************************************************/
/* Downloads texts                                   */
/*****************************************************/

define("_FILEINFO","Filinformasjon");
define("_CHECK","Check");
define("_AUTHORNAME","Author's Name");
define("_AUTHOREMAIL","Author's Email");
define("_DOWNLOADNAME","Programtittel");
define("_ADDTHISFILE","Add this file");
define("_INBYTES","in bytes");
define("_PROGRAMNAME","Programnavn");
define("_FILESIZE","Filst�rrelse");
define("_VERSION","Versjon");
define("_UPLOADDATE","Upload dato");
define("_DESCRIPTION","Beskrivelse");
define("_UDOWNLOADS","Downloads");
define("_AUTHOR","Forfatter");
define("_HOMEPAGE","Hjemmeside");
define("_DOWNLOADNOW","Last ned denne filen n�!");
define("_SELECTCATEGORY","Velg kategorimappe");
define("_ALLFILES","Alle filene");
define("_INFO","Info");
define("_DISPLAYFILTERED","Vis filtrert etter");
define("_SORTED","sortert");
define("_ASCENDING","stigende");
define("_DESCENDING","synkende");
define("_NAME","Navn");
define("_CREATIONDATE","Opprettet dato");
define("_DOWNLOADSECTION","Download seksjon");
define("_NOSUCHFILE","Filen eksisterer ikke...");
define("_RATERESOURCE","Vurder Download");
define("_FILEURL","Fil URL");
define("_ADDDOWNLOAD","Legg til fil");
define("_DOWNLOADSMAIN","Downloads Hovedside");
define("_DOWNLOADCOMMENTS","Download kommentarer");
define("_DOWNLOADSMAINCAT","Downloads hovedkategorier");
define("_ADDADOWNLOAD","Legg til ny fil");
define("_DSUBMITONCE","Bare legg til unik download en gang.");
define("_DPOSTPENDING","Alle filer til nedlasting blir sendt til verifisering.");
define("_RESSORTED","Resources currently sorted by");
define("_DOWNLOADSNOTUSER1","Du er ikke registrert, eller s� har du ikke logget inn.");
define("_DOWNLOADSNOTUSER2","Dersom du hadde v�rt registrert s� kunne du ha lagt til filer for download p� dette webstedet.");
define("_DOWNLOADSNOTUSER3","Det er en enkel og kjapp prosess � registrere seg.");
define("_DOWNLOADSNOTUSER4","Hvorfor forlanger vi at folk registrerer seg (gratis) for � f� tilgang til visse funksjoner?");
define("_DOWNLOADSNOTUSER5","S� vi kan tilby deg det beste innholdet,");
define("_DOWNLOADSNOTUSER6","hver eneste ting blir individuelt behandlet og godkjent av de ansvarlige.");
define("_DOWNLOADSNOTUSER7","Vi pr�ver � tilby deg mest mulig verdifull informasjon.");
define("_DOWNLOADSNOTUSER8","<a href=\"user.php\">Registrer deg gratis</a>");
define("_DOWNLOADALREADYEXT","FEIL: Denne URLen eksisterer allerede i databasen!");
define("_DOWNLOADNOTITLE","FEIL: Du mangler en TITTEL for filen!");
define("_DOWNLOADNOURL","FEIL: Du glemte � skrive URL til filen!");
define("_DOWNLOADNODESC","FEIL: Du m� oppgi en BESKRIVELSE for filen din!");
define("_DOWNLOADRECEIVED","Vi mottok din download");
define("_NEWDOWNLOADS","Nye downloads");
define("_TOTALNEWDOWNLOADS","Antall nye downloads");
define("_DTOTALFORLAST","Antall nye downloads siste");
define("_DBESTRATED","Best vurderte downloads - Topp");
define("_TRATEDDOWNLOADS","Antall vurderte downloads");
define("_DLALSOAVAILABLE","Downloads er ogs� tilgjengelig i");
define("_SORTDOWNLOADSBY","Sorter downloads etter");
define("_DCATNEWTODAY","Ny fil(er) lagt til denne katalogen i dag");
define("_DCATLAST3DAYS","Ny fil(er) lagt til denne katalogen i l�pet av de siste 3 dagene");
define("_DCATTHISWEEK","Ny fil(er) lagt til denne katalogen denne uken");
define("_DDATE1","Dato (Eldste filer listet f�rst)");
define("_DDATE2","Dato (Nyeste filer listet f�rst)");
define("_DOWNLOADS","Downloads");
define("_DOWNLOADPROFILE","Download profil");
define("_DOWNLOADRATINGDET","Detaljert downloadvurdering");
define("_EDITTHISDOWNLOAD","Rediger denne filen");
define("_DOWNLOADRATING","Download vurdering");
define("_DOWNLOADVOTE","Stem!");
define("_ONLYREGUSERSMODIFY","Bare registrerte brukere kan foresl� modifikasjoner til linker. Vennligst <a href=\"user.php\">registrer deg eller logg inn</a>.");
define("_REQUESTDOWNLOADMOD","Foresl� download modifikasjon");
define("_DOWNLOADID","Download ID");
define("_DLETSDECIDE","Input fra brukere som deg selv hjelper andre bes�kende til lettere � bestemme hva de vil laste ned.");
define("_DRATENOTE4","Her har du lista over de <a href=\"downloads.php?op=TopRated\">beste resursene</a>.");

/*****************************************************/
/* Private Messages texts                            */
/*****************************************************/

define("_PRIVATEMESSAGES","Dine private meldinger");
define("_CHECKALL","Velg alle");
define("_FROM","Fra");
define("_DATE","Dato");
define("_DONTHAVEMESSAGES","Du har ingen beskjeder");
define("_NOTREAD","Ikke lest");
define("_MSGSTATUS","Meldingsstatus");
define("_PRIVATEMESSAGE","Private meldinger");
define("_INDEX","Index");
define("_POSTS","Poster");
define("_SENT","Sendt");
define("_PREVIOUSMESSAGE","Forrige melding");
define("_NEXTMESSAGE","Neste melding");
define("_PROFILE","Profil");
define("_ABOUTPOSTING","Om posting");
define("_ALLREGCANPOST","Alle registrerte brukere kan poste private meldinger");
define("_TO","Til");
define("_MESSAGEICON","Meldingsikon");
define("_MESSAGE","Melding");
define("_HTML","HTML");
define("_PMON","P�");
define("_PMOFF","Av");
define("_BBCODE","BBCode");
define("_WROTE","skrev");
define("_OPTIONS","Valg");
define("_HTMLDISSABLE","Koble ut HTML p� denne posten");
define("_BBCODEDISSABLE","Koble ut <a href=\"bbcode_ref.php\" target=\"_blank\"><i>BBCode</i></a> i denne meldinga");
define("_SMILEDISSABLE","Koble ut <a href=\"bb_smilies.php\" target=\"_blank\"><i>smilies</i></a> i denne meldinga");
define("_SHOWSIGNATURE","Vis signatur");
define("_SIGNATUREMSG","Dette kan forandres eller legges til i profilen din");
define("_CANCELREPLY","kanseller svar");
define("_CANCELSEND","Kanseller posting");
define("_CLEAR","T�m");
define("_SUBMIT","Send inn");
define("_USERNOTINDB","Valgt bruker finnes ikke i databasen.");
define("_CHECKNAMEANDTRY","Vennligst sjekk navnet, og pr�v igjen.");
define("_MSGPOSTED","Din melding har blir postet.");
define("_RETURNTOPMSG","Du kan klikke her for � oppdatere dine private meldinger.");
define("_MSGDELETED","Dine meldinger har blitt slettet.");

/*****************************************************/
/* Web Links Messages texts                          */
/*****************************************************/

define("_ADDLINK","Legg til link");
define("_NEW","Nye");
define("_POPULAR","Popul�r");
define("_TOPRATED","Toppsjiktet");
define("_RANDOM","Tilfeldig link");
define("_LINKSMAIN","Linkoversikten");
define("_LINKCOMMENTS","Link kommentarer");
define("_ADDITIONALDET","Detaljer");
define("_EDITORREVIEW","Editor gjennomgang");
define("_REPORTBROKEN","Rapporter d�d link");
define("_LINKSMAINCAT","Hovedkategorier");
define("_AND","og");
define("_INDB","i databasen v�r");
define("_ADDALINK","Legg til ny link");
define("_INSTRUCTIONS","Instruksjoner");
define("_SUBMITONCE","Send unike linker bare en gang");
define("_POSTPENDING","Alle linker blir postet til systemet i p�vente av bekreftelse.");
define("_USERANDIP","Brukernavn og IP er lagret, s� vennligst ikke misbruk systemet.");
define("_PAGETITLE","Tittel");
define("_PAGEURL","URL");
define("_YOUREMAIL","Din epost");
define("_LDESCRIPTION","Beskrivelse: (255 tegn maks)");
define("_ADDURL","Legg til denne linken");
define("_LINKSNOTUSER1","Du er enten ikke registrert, eller s� har du ikke logget inn.");
define("_LINKSNOTUSER2","Om du var registrert s� kunne du ha lagt til linker til denne websiten.");
define("_LINKSNOTUSER3","Registrering er en enkel og grei prosess.");
define("_LINKSNOTUSER4","Hvorfor krever vi at du registrerer deg for � aksessere noen funksjoner?");
define("_LINKSNOTUSER5","Slik at vi kan tilby deg best mulig kvalitet,");
define("_LINKSNOTUSER6","hver post blir sjekket og godkjent individuelt av portalansvarlige.");
define("_LINKSNOTUSER7","Vi pr�ver � bare tilby deg verdifull informasjon.");
define("_LINKSNOTUSER8","<a href=\"user.php\">Registrer deg gratis</a>");
define("_LINKALREADYEXT","FEIL: Denne URL'en finnes allerede i databasen!");
define("_LINKNOTITLE","FEIL: Du m� oppgi en TITTEL for din URL!");
define("_LINKNOURL","FEIL: Du m� skrive inn URL'en !");
define("_LINKNODESC","FEIL: Du m� lage en BESKRIVELSE for din URL!");
define("_LINKRECEIVED","Vi har mottatt din link. Takk!");
define("_EMAILWHENADD","Du vil motta en epost, n�r den er godkjent.");
define("_CHECKFORIT","Du skrev ingen epostadresse. Vi vil uansett sjekke linken din snart.");
define("_NEWLINKS","Nye linker");
define("_TOTALNEWLINKS","Antall nye linker");
define("_LASTWEEK","Forrige uke");
define("_LAST30DAYS","Siste 30 dager");
define("_1WEEK","1 uke");
define("_2WEEKS","2 uker");
define("_30DAYS","30 dager");
define("_SHOW","Vis");
define("_TOTALFORLAST","Nye linker siste");
define("_DAYS","dager");
define("_ADDEDON","Lagt til");
define("_VOTE","Stem");
define("_VOTES","Stemmer");
define("_RATING","Vurdering");
define("_RATESITE","Vurder denne siten");
define("_DETAILS","Detaljer");
define("_BESTRATED","Best Rated Linker - Topp *sjekk*");
define("_OF","av");
define("_TRATEDLINKS","linker vurdert totalt");
define("_TVOTESREQ","stemmer som trengs");
define("_SHOWTOP","Vis topp");
define("_MOSTPOPULAR","Mest popul�re - Topp");
define("_OFALL","av alle");
define("_LALSOAVAILABLE","Linker ogs� tilgjengelig i");
define("_SUBCATEGORIES","underkategorier");
define("_SORTLINKSBY","Sorter linker etter");
define("_SITESSORTED","Sitene er n� sortert etter");
define("_POPULARITY","Popularitet");
define("_SELECTPAGE","Velg side");
define("_MAIN","Hovedmeny");
define("_NEWTODAY","Nye i dag");
define("_NEWLAST3DAYS","Nye siste 3 dager");
define("_NEWTHISWEEK","Nye denne uken");
define("_CATNEWTODAY","Nye linker i denne kategorien - lagt til i dag");
define("_CATLAST3DAYS","Nye linker i denne kategorien - lagt til de siste 3 dagene");
define("_CATTHISWEEK","Nye linker i denne kategorien - lagt til denne uka");
define("_POPULARITY1","Popularitet (F�rrest til flest hits)");
define("_POPULARITY2","Popularitet (Flest til f�rrest hits)");
define("_TITLEAZ","Tittel (A til Z)");
define("_TITLEZA","Tittel (Z til A)");
define("_DATE1","Dato (Gamle linker listet f�rst)");
define("_DATE2","Dato (Nye linker listet f�rst)");
define("_RATING1","Vurdering (Laveste resultat til h�yeste resultat)");
define("_RATING2","Vurdering (H�yeste resultat til laveste resultat)");
define("_SEARCHRESULTS4","S�keresultater for");
define("_USUBCATEGORIES","Underkategorier");
define("_LINKS","Linker");
define("_TRY2SEARCH","Pr�v � s�ke");
define("_INOTHERSENGINES","p� andre S�kemaskiner");
define("_EDITORIAL","Editorial *sjekk*");
define("_LINKPROFILE","Lenkeprofil");
define("_EDITORIALBY","redaksjonel artikkel av");
define("_NOEDITORIAL","Ingen seksjonsdokumenter er tilgjengelig akkurat n� p� denne websiten.");
define("_VISITTHISSITE","Bes�k denne websiten");
define("_RATETHISSITE","Vurder denne websiten");
define("_ISTHISYOURSITE","Er dette din ressurs");
define("_ALLOWTORATE","La andre brukere stemme p� denne fra din webside! *sjekk*");
define("_LINKRATINGDET","Linkenes vurderingsdetaljer");
define("_OVERALLRATING","Samlet vurdering");
define("_TOTALOF","Totalt av");
define("_USER","Bruker");
define("_USERAVGRATING","Brukers gjennomsnittlige vurdering");
define("_NUMRATINGS","# vurderinger");
define("_EDITTHISLINK","Rediger denne linken");
define("_REGISTEREDUSERS","Registrerte brukere");
define("_NUMBEROFRATINGS","Antall vurderinger");
define("_NOREGUSERSVOTES","Inge registrerte brukerstemmer");
define("_BREAKDOWNBYVAL","Analyse av vurdering per verdi");
define("_LTOTALVOTES","antall stemmer totalt");
define("_LINKRATING","Linker - Vurdering");
define("_HIGHRATING","H�y vurdering");
define("_LOWRATING","Lav vurdering");
define("_NUMOFCOMMENTS","Antall kommentarer");
define("_WEIGHNOTE","* NB: Denne websiden veier registrerte mot uregistrerte brukeres stemmer");
define("_NOUNREGUSERSVOTES","Ingen stemmer fra uregistrerte brukere");
define("_WEIGHOUTNOTE","* NB: Denne websiten  veier registrerte stemmer mot stemmer fra \"utsiden\" (andre websider)");
define("_NOOUTSIDEVOTES","Ingen stemmer fra utsiden");
define("_OUTSIDEVOTERS","Stemmer fra utsiden");
define("_UNREGISTEREDUSERS","Uregistrerte brukere");
define("_PROMOTEYOURSITE","Promoter din website");
define("_PROMOTE01","Kanskje du er interessert i noen av de forskjellige eksterne 'Vurder en webside' valgene vi har. De lar deg legge inn et bilde (eller en vurderingsskjema) p� hjemmesiden din, slik at du f�r flere stemmer p� websiden din. Vennligst gj�r et valg nedenfor:");
define("_TEXTLINK","Tekst link");
define("_PROMOTE02","En m�te � linke til vurderingen er gjennom en enkel tekstlink:");
define("_HTMLCODE1","HTML koden du skal bruke i s� m�te er f�lgende:");
define("_THENUMBER","Nummeret");
define("_IDREFER","i HTML koden refererer til din websides ID nummer in $sitename databasen. Du m� forsikre deg at dette nummeret er tilstedet.");
define("_BUTTONLINK","Button link");
define("_PROMOTE03","Dersom du ser etter noe litt mer enn en enkel tekst link, s� kan du bruke en liten button link:");
define("_RATEIT","Vurder denne site'n!");
define("_HTMLCODE2","Kildekoden for knappen over er");
define("_REMOTEFORM","Vurderingsskjema");
define("_PROMOTE04","Dersom du jukser med dette, vil linken til din webside bli fjernet.  N� som det er sagt, slik ser et vurderingsskjema ut:");
define("_VOTE4THISSITE","Vurder denne websiden!");
define("_LINKVOTE","Stem!");
define("_HTMLCODE3","Bruker du dette skjemaet kan brukerne dine vurdere websiden din direkte fra siden din og resultatet vil bli registrert her. Skjemaet over er ikke aktivert, men f�lgende kildekode vil fungere om du gj�r en enkel klipp og lim inn p� din side. Kildekoden er vist under:");
define("_PROMOTE05","Tusen takk! Lykke til med vurderingene!");
define("_STAFF","Personalet *sjekk*");
define("_THANKSBROKEN","Takk for at du hjelper til med � vedlikeholde denne katalogens integritet.");
define("_SECURITYBROKEN","Ditt brukernavn og IP vil blir registrert midlertidig for sikkerhetsmessige �rsaker.");
define("_THANKSFORINFO","Takk for informasjonen.");
define("_LOOKTOREQUEST","Vi vil se p� din foresp�rsel s� snart som mulig.");
define("_ONLYREGUSERSMODIFY","Only registered users can suggest links modifications. Please <a href=\"user.php\">register or login</a>.");
define("_REQUESTLINKMOD","Foresl� modifisering til link");
define("_LINKID","Link ID");
define("_SENDREQUEST","Send foresp�rsel");
define("_THANKSTOTAKETIME","Takk for at du tar deg tid til � vurdere en webside her p�");
define("_LETSDECIDE","Input fra brukere slik som deg selv vil hjelpe andre bes�kende til � finne de linkene de vil klikke p�");
define("_WEAPPRECIATE","Vi verdsetter ditt bes�k p� ");
define("_RETURNTO","Returner til");
define("_RATENOTE1","Vennligst ikke stem p� samme resurs mer enn en gang.");
define("_RATENOTE2","Skalaen er fra 1 til 10, hvor 1 er d�rligst og 10 er best.");
define("_RATENOTE3","Vennligst v�r objektiv i vurderingen din. Dersom alle gir 1 eller 10, vil ikke vurderingen ha noe verdi.");
define("_RATENOTE4","Du kan se en liste over de <a href=\"links.php?op=TopRated\">topp vurderte websidene</a>.");
define("_RATENOTE5","Ikke stemm p� din egen eller en konkurent sin side.");
define("_YOUAREREGGED","Du er registrert og logget inn.");
define("_FEELFREE2ADD","Legg til en kommentar om denne siten.");
define("_YOUARENOTREGGED","Du er enten ikke registrert, eller s� har du ikke logget inn.");
define("_IFYOUWEREREG","Du kunne ha lagt inn kommentarer dersom du var registrert p� denne websiten.");

/*****************************************************/
/* Messages System texts                             */
/*****************************************************/

define("_MVIEWADMIN","Vis: Bare administrator");
define("_MVIEWUSERS","Vis: Bare registrerte bes�kende");
define("_MVIEWANON","Vis: Bare anonyme bes�kende");
define("_MVIEWALL","Vis: Alle bes�kende");
define("_EXPIRELESSHOUR","G�r ut: Mindre enn 1 time");
define("_EXPIREIN","G�r ut i");

/*****************************************************/
/* Administration Messages texts                     */
/*****************************************************/

define("_FILEMANAGER","Filbehandler");
define("_CURRENTDIR","Aktiv katalog er");
define("_BACKTOROOT","Tilbake til root");
define("_REFRESH","Oppdater");
define("_ADMINID","Admin ID");
define("_ADMINLOGIN","Administrator System Login");
define("_FAQ","FAQ");
define("_DOWNLOAD","Download");
define("_HEADLINES","Overskrifter");
define("_LEFTBLOCKS","Venstre rammer");
define("_WEBLINKS","Weblinker");
define("_MAINBLOCK","Hovedmeny");
define("_EDITADMINS","Administratorer");
define("_ADMINBLOCK","Admin rammen");
define("_HTTPREFERERS","HTTP referanter");
define("_PREFERENCES","Oppsett");
define("_ADMPOLLS","Avstemninger");
define("_RIGHTBLOCKS","H�yre rammer");
define("_SECTIONSMANAGER","Seksjonsdokumenter");
define("_ADDSTORY","Legg til artikkel");
define("_AUTOARTICLES","Automatiske artikler");
define("_EDITUSERS","Brukere");
define("_ADMINMENU","Administrasjonsmeny");
define("_BANNERSADMIN","Banneradministrasjon");
define("_ONLINEMANUAL","Online Manual");
define("_ADMINLOGOUT","Logg ut / Avslutt");
define("_LAST","Siste");
define("_GO","Ok");
define("_CURRENTPOLL","Aktiv avstemning");
define("_STORYID","Artikkel ID");

/*****************************************************/
/* Banners Administration texts                      */
/*****************************************************/

define("_ACTIVEBANNERS","Current Active Banners");
define("_ACTIVEBANNERS2","Active Banners");
define("_IMPRESSIONS","Impressions");
define("_IMPLEFT","Imp. Left");
define("_CLICKS","Clicks");
define("_CLICKSPERCENT","% Clicks");
define("_CLIENTNAME","Client Name");
define("_UNLIMITED","Unlimited");
define("_FINISHEDBANNERS","Finished Banners");
define("_IMP","Imp.");
define("_DATESTARTED","Date Started");
define("_DATEENDED","Date Ended");
define("_ADVERTISINGCLIENTS","Advertising Clients");
define("_CONTACTNAME","Contact Name");
define("_CONTACTEMAIL","Contact Email");
define("_ADDNEWBANNER","Add a New Banner");
define("_PURCHASEDIMPRESSIONS","Purchased Impressions");
define("_IMAGEURL","Image URL");
define("_CLICKURL","Click URL");
define("_ADDBANNER","Add Banner");
define("_ADDCLIENT","Add a New Client");
define("_CLIENTLOGIN","Client Login");
define("_CLIENTPASSWD","Client Password");
define("_ADDCLIENT2","Add Client");
define("_BANNERSNOTACTIVE","Banners variable in config.php file is set to 0, this means that you can't see your Banners until you activate it.");
define("_TOACTIVATE","To do this, just change the value from <a href=\"admin.php?op=Configure#banners\">settings section</a>.");
define("_IMPORTANTNOTE","Important Note!");
define("_DELETEBANNER","Delete Banner");
define("_SURETODELBANNER","Are you sure you want to delete this Banner?");
define("_EDITBANNER","Edit Banner");
define("_ADDIMPRESSIONS","Add More Impressions");
define("_PURCHASED","Purchased");
define("_MADE","Made");
define("_DELETECLIENT","Delete Advertising Client");
define("_SURETODELCLIENT","You are about to delete the client and all its Banners!!!");
define("_CLIENTWITHOUTBANNERS","This client doesn't have any banner running now.");
define("_DELCLIENTHASBANNERS","This client has the following ACTIVE BANNERS running now");
define("_EDITCLIENT","Edit Advertising Client");

/*****************************************************/
/* Comments Administration texts                     */
/*****************************************************/

define("_REMOVECOMMENTS","Slett kommentarer");
define("_SURETODELCOMMENTS","Er du sikker p� at du vil slette valgt kommentar og alle dens svar?");

/*****************************************************/
/* Blocks Administration texts                       */
/*****************************************************/

define("_BLOCKSADMIN","Administrer blokkene");
define("_BLOCKS","Blokker");
define("_FIXEDBLOCKS","Faste system blokker");
define("_TITLE","Tittel");
define("_POSITION","Posisjon");
define("_WEIGHT","Vekt");
define("_STATUS","Status");
define("_LEFTBLOCK","Venstre blokk");
define("_LEFT","Venstre");
define("_RIGHTBLOCK","H�yre blokk");
define("_RIGHT","H�yre");
define("_ACTIVE","Aktive");
define("_DEACTIVATE","Deaktivere");
define("_INACTIVE","Innaktive");
define("_ACTIVATE","Aktivere");
define("_USERBLOCKS","Brukerdefinerte blokker");
define("_TYPE","Type");
define("_ADDNEWBLOCK","Legg til ny blokk");
define("_RSSFILE","RSS/RDF fil URL");
define("_ONLYHEADLINES","(Bare for overskrifter)");
define("_CONTENT","Innhold");
define("_ACTIVATE2","Aktivere?");
define("_REFRESHTIME","Refresh tid");
define("_HOUR","Time");
define("_HOURS","Timer");
define("_CREATEBLOCK","Opprett blokk");
define("_EDITBLOCK","Redigere blokk");
define("_BLOCK","Blokk");
define("_SAVEBLOCK","Lagre blokk");
define("_EDITFIXEDBLOCK","Edit Fixed Block");
define("_BLOCKACTIVATION","Blokkaktivering");
define("_BLOCKPREVIEW","Dette er en forh�ndsvisning av blokken");
define("_WANT2ACTIVATE","Vil du aktivere denne blokken?");
define("_ARESUREDELBLOCK","Er du sikker p� at du vil fjerne denne blokken");
define("_RSSFAIL","Det er et problem med RSS fil URLen");
define("_RSSTRYAGAIN","Vennligst sjekk URL og RSS filnavnet, og pr�v igjen.");
define("_RSSPROBLEM","Vi har for tiden problemer med � vise overskriftene fra denne websiden");
define("_RSSCONTENT","(RSS/RDF innhold)");
define("_IFRSSWARNING","Dersom du fyller inn en URL s� vil ikke det du skriver bli vist!");
define("_SELECTLANGUAGE","Velg spr�k");
define("_SELECTGUILANG","Velg spr�k:");
define("_BLOCKUP","Blokk OPP");
define("_BLOCKDOWN","Block NED");
define("_SETUPHEADLINES","(Velg Custom og skriv URLen eller bare velg en webside fra listen for � hente inn nyhetsoversikt)");
define("_HEADLINESADMIN","Administrer overskrifter");
define("_ADDHEADLINE","Legg til overskrift");
define("_EDITHEADLINE","Rediger overskrifer");
define("_SURE2DELHEADLINE","ADVARSEL: Er du sikker p� at du vil slette denne overskriften?");
define("_CUSTOM","Custom");

/*****************************************************/
/* FAQ Administration texts                          */
/*****************************************************/

define("_FAQADMIN","FAQ administrasjon");
define("_ACTIVEFAQS","Aktive FAQs");
define("_ADDCATEGORY","Legg til ny kategori");
define("_QUESTIONS","Sp�rsm�l og svar");
define("_ADDQUESTION","Legg til nytt sp�rsm�l");
define("_QUESTION","Sp�rsm�l");
define("_ANSWER","Svar");
define("_EDITCATEGORY","Rediger kategori");
define("_EDITQUESTIONS","Rediger sp�rsm�l og svar");
define("_FAQDELWARNING","ADVARSEL: Er du sikker p� at du vil slette denne FAQ'n og alt innhold? *sjekk**kategori?*");
define("_QUESTIONDEL","ADVARSEL: Er du sikker p� at du vil slette dette sp�rsm�let??");

/*****************************************************/
/* Author's Administration texts                     */
/*****************************************************/

define("_AUTHORSADMIN","Administrasjon av ansvarlige");
define("_MODIFYINFO","Endre Info");
define("_DELAUTHOR","Slett ansvarlig");
define("_ADDAUTHOR","Legg til ny administrator");
define("_PERMISSIONS","Rettigheter");
define("_TOPICS","Kategorier");
define("_USERS","Brukere");
define("_SURVEYS","Avstemning");
define("_SECTIONS","Avsnitt");
define("_SUPERUSER","Superbruker");
define("_SUPERWARNING","ADVARSEL: Dersom Superbruker er avkrysset vil brukeren f� full tilgang!");
define("_ADDAUTHOR2","Legg til forfatter ");
define("_RETYPEPASSWD","Gjenta Passord");
define("_FORCHANGES","(Bare for endringer)");
define("_COMPLETEFIELDS","Du m� fylle ut alle de p� p�budte feltene");
define("_CREATIONERROR","FEIL ved oppretting av ansvarlig");
define("_AUTHORDELSURE","Er du sikker p� at du vil slette");
define("_AUTHORDEL","Slett ansvarlig");
define("_REQUIREDNOCHANGE","(p�budt, kan ikke forandres senere)");
define("_PUBLISHEDSTORIES","Denne ansvarlige har pubslisert artikler");
define("_SELECTNEWADMIN","Please select a new admin to re-assign them *translate*");
define("_GODNOTDEL","*(GOD kontoen kan ikke slettes)");
define("_MAINACCOUNT","God Admin*");

/*****************************************************/
/* Articles/Stories Administration texts             */
/*****************************************************/

define("_ARTICLEADMIN","Administrasjon - artikler/nyheter");
define("_ADDARTICLE","Legg til ny artikkel");
define("_STORYTEXT","Artikkeltekst");
define("_EXTENDEDTEXT","Utvidet tekst");
define("_ARESUREURL","(La du til en URL? Sjekket du den for skrivefeil?)");
define("_PUBLISHINHOME","Publiser p� forsiden?");
define("_ONLYIFCATSELECTED","Virker bare dersom <i>Artikler</i> kategorien ikke er valgt");
define("_ADD","Legg til");
define("_PROGRAMSTORY","Vil du programere denne artiklen?");
define("_NOWIS","N� er");
define("_DAY","Dag");
define("_UMONTH","M�ned");
define("_YEAR","�r");
define("_PREVIEWSTORY","Forh�ndsvis artikkel");
define("_POSTSTORY","Post artikkel");
define("_AUTOMATEDARTICLES","Programmerte artikkler");
define("_NOAUTOARTICLES","Det finnes ingen programerte artikler");
define("_REMOVESTORY","Er du sikker p� at du vil fjerne artikel ID #");
define("_ANDCOMMENTS","og alle dens kommentarer?");
define("_CATEGORIESADMIN","Kategoriadministrasjon");
define("_CATEGORYADD","Legg til ny kategori");
define("_CATNAME","Kategorinavn");
define("_NOARTCATEDIT","Du kan ikke redigere <i>Artikkel</i> kategorien");
define("_ASELECTCATEGORY","Vel kategori");
define("_CATEGORYNAME","Kategorinavn");
define("_DELETECATEGORY","Slett kategori");
define("_SELECTCATDEL","Velg en kategori du vil slette");
define("_CATDELETED","Kategorien er slettet!");
define("_WARNING","ADVARSEL");
define("_THECATEGORY","Kategorien");
define("_HAS","har");
define("_STORIESINSIDE","artikler tilknyttet");
define("_DELCATWARNING1","Du kan slette denne kategorien og alle artiklene og kommentarene");
define("_DELCATWARNING2","eller du kan FLYTTE alle artiklene til en ny kategori.");
define("_DELCATWARNING3","Hva vil du gj�re?");
define("_YESDEL","Ja! Slett ALLE!");
define("_NOMOVE","Nei! Flytt artiklene mine");
define("_MOVESTORIES","Flytt artiklene til en ny kategori");
define("_ALLSTORIES","ALLE artiklene under");
define("_WILLBEMOVED","vil bli flyttet.");
define("_SELECTNEWCAT","Vennligst velg en ny kategori");
define("_MOVEDONE","Gratulerer! Flyttingen er fullf�rt!");
define("_CATEXISTS","Kategorien eksisterer allerede!");
define("_CATSAVED","Kategorien er lagret *sjekk*!");
define("_GOTOADMIN","G� til admin seksjonen");
define("_CATADDED","Ny kategori er lagt til!");
define("_AUTOSTORYEDIT","Rediger programert artikkel");
define("_NOTES","Bemerkninger");
define("_CHNGPROGRAMSTORY","Velg ny dato for denne artiklen:");
define("_SUBMISSIONSADMIN","Administrer innsendte artikler");
define("_DELETESTORY","Slett artikkel");
define("_EDITARTICLE","Rediger artikkel");
define("_NOSUBMISSIONS","Ingen nye innlegg");
define("_NEWSUBMISSIONS","Innsendte artikler");
define("_NOFUNCTIONS","---------");
define("_NOTAUTHORIZED1","Du har ikke adgang til � r�re denne artiklene!");
define("_NOTAUTHORIZED2","Du kan ikke redigere og/eller slette artikler du ikke har publisert");

/*****************************************************/
/* HTTP Referers Administration texts                */
/*****************************************************/

define("_WHOLINKS","Hvem linker til v�r site?");
define("_DELETEREFERERS","Slett referanter");

/*****************************************************/
/* Polls/Surveys Administration texts                */
/*****************************************************/

define("_POLLSADMIN","Administrer avstemninger");
define("_CREATEPOLL","Opprett ny avstemning");
define("_DELETEPOLLS","Slett avstemninger");
define("_POLLTITLE","Avstemningstittel");
define("_POLLEACHFIELD","Fyll ut et valg per felt.");
define("_CREATEPOLLBUT","Opprett avstemning");
define("_REMOVEEXISTING","Fjern en eksisterende avstemning");
define("_POLLDELWARNING","ADVARSEL: Den valgte avstemning vil bli fjernet �YEBLIKKELIG fra databasen!");
define("_CHOOSEPOLL","Vennligst velg en avstemning fra listen under:");

/*****************************************************/
/* Topics Manager Administration texts               */
/*****************************************************/

define("_TOPICSMANAGER","H�ndtering av emner");
define("_CURRENTTOPICS","Aktive emner");
define("_CLICK2EDIT","Klikk p� emne for � redigere");
define("_ADDATOPIC","Add a New Topic");
define("_TOPICNAME","Emne nanvn");
define("_TOPICNAME1","(kun et navn uten mellomrom - maks: 20 tegn)");
define("_TOPICNAME2","(for eksempel: spilloghobbier)");
define("_TOPICTEXT","Emne tekst");
define("_TOPICTEXT1","(hele emneteksten eller beskrivelse - maks: 40 tegn)");
define("_TOPICTEXT2","(for eksempel: Spill og hobbier)");
define("_TOPICIMAGE","Emne bilde");
define("_TOPICIMAGE1","emne bilde + etternavn lokaliser i");
define("_TOPICIMAGE2","(for eksempel: spill.gif)");
define("_ADDTOPIC","Legg til emne");
define("_EDITTOPIC","Rediger emne");
define("_ADDRELATED","Legg til relatert link");
define("_ACTIVERELATEDLINKS","Relaterte linker");
define("_SITENAME","Websidens navn");
define("_NORELATED","Det er ikke noen relaterte linker til dette emnet");
define("_EDITRELATED","Rediger relaterte linker");
define("_DELETETOPIC","Slett emne");
define("_TOPICDELSURE","Er du sikker p� at du vil slette emnet");
define("_TOPICDELSURE1","Dette vil slette ALLE dens artikler og kommentarer!");

/*****************************************************/
/* User's Administration texts                       */
/*****************************************************/

define("_USERADMIN","Administrer brukere");
define("_EDITUSER","Rediger brukere");
define("_MODIFY","Endre");
define("_ADDUSER","Legg til ny bruker");
define("_FAKEEMAIL","Falsk epost");
define("_ALLOWUSERS","La andre brukere se din epost adresse");
define("_ADDUSERBUT","Opprett bruker");
define("_USERUPDATE","Oppdater bruker");
define("_USERID","Bruker ID");
define("_USERNOEXIST","Bruker eksisterer ikke!");
define("_PASSWDNOMATCH","Beklager, passordet stemmer ikke overens. G� tilbake og pr�v igjen!");
define("_DELETEUSER","Slett bruker");
define("_SURE2DELETE","Er du sikker p� at du vil slette brukeren");
define("_NEEDTOCOMPLETE","Du m� fylle ut p�budte felt");

/*****************************************************/
/* Settings Administration texts                     */
/*****************************************************/

define("_SITECONFIG","Websidens oppsett");
define("_GENSITEINFO","Generell webside info");
define("_SITEURL","Site URL");
define("_SITELOGO","Site Logo");
define("_SITESLOGAN","Site slogan");
define("_ADMINEMAIL","Administrator epost");
define("_ITEMSTOP","Antall poster p� \"Topp XX\" siden");
define("_STORIESHOME","Artikler p� forsiden");
define("_OLDSTORIES","Artikkeloverskrifter i \"gamle artikler\" boksen");
define("_ACTULTRAMODE","Aktiver ultramode?");
define("_DEFAULTTHEME","Standard Theme for websiden");
define("_SELLANGUAGE","Velg standard spr�k for websiden");
define("_LOCALEFORMAT","Lokalt tidsformat");
define("_BANNERSOPT","Bannervalg");
define("_STARTDATE","Websidens oppstartdato");
define("_ACTBANNERS","Aktiver bannere p� din side?");
define("_YOURIP","Din IP telles ikke");
define("_FOOTERMSG","Footer beskjed");
define("_FOOTERLINE1","Footer linje 1");
define("_FOOTERLINE2","Footer linje 2");
define("_FOOTERLINE3","Footer linje 3");
define("_FOOTERLINE4","Footer linje 4");
define("_BACKENDCONF","Backend konfigurasjon");
define("_BACKENDTITLE","Backend tittel");
define("_BACKENDLANG","Backend spr�k");
define("_WEBLINKSCONF","Weblinker - standard konfigurasjon");
define("_LINKSPAGE","Linker per side");
define("_TOBEPOPULAR","Hits for � v�re popul�r");
define("_LINKSASNEW","Antall linker som nye");
define("_LINKSASBEST","Antall linker som beste");
define("_LINKSINRES","Linker i s�keresultatet");
define("_ANONPOSTLINKS","La anonyme brukere legge til nye linker?");
define("_MAIL2ADMIN","Post nye artilker til admin");
define("_NOTIFYSUBMISSION","Beskjed om nye innlegg via epost?");
define("_EMAIL2SENDMSG","Epostadresse for mottaker");
define("_EMAILSUBJECT","Epost emne");
define("_EMAILMSG","Epost melding");
define("_EMAILFROM","Epostkonto (Fra)");
define("_COMMENTSMOD","Kommentar moderasjon");
define("_MODTYPE","Moderasjonstype");
define("_MODADMIN","Moderasjon av admin");
define("_MODUSERS","Moderasjon av brukere");
define("_NOMOD","Ingen Moderasjon");
define("_COMMENTSOPT","Kommentaroppsett");
define("_COMMENTSLIMIT","Kommentargrense i bytes");
define("_ANONYMOUSNAME","Standard \"anonym\" navn");
define("_SURVEYSCONF","Avstemningsoppsett");
define("_SCALEBAR","Skala av resultbar");
define("_ALLOWTWICE","Tillat brukere � stemme to ganger?");
define("_GRAPHICOPT","Grafikkoppsett");
define("_TOPICSPATH","Sti til emne-bilder");
define("_USERPATH","Sti til bruker-bilder");
define("_ADMINPATH","Sti til adminmeny-bilder");
define("_ADMINGRAPHIC","Grafikk i administrasjon menyen?");
define("_SITEFONT","Websidens font");
define("_MISCOPT","Diverse valg");
define("_ARTINADMIN","Artikkler p� adminsiden");
define("_PASSWDLEN","Minimum lengde p� brukerpassord");
define("_MAXREF","Hvor mange refereranter �nsker du som maksimum?");
define("_COMMENTSPOLLS","Aktiver kommentarer p� avstemmninger?");
define("_EPHEMACT","Aktiver begivenheter");
define("_ALLOWANONPOST","Tillat anonyme � poste?");
define("_ACTIVATEHTTPREF","Aktivere HTTP Referenter?");

/*****************************************************/
/* Sections Administration texts                     */
/*****************************************************/

define("_SECTIONSADMIN","Administrer seksjoner");
define("_ACTIVESECTIONS","Aktive seksjonskategorier");
define("_CLICK2EDITSEC","(Klikk for � redigere)");
define("_ADDSECARTICLE","Legg til ny redaksjonell artikkel");
define("_EDITARTID","Rediger artikkel ID");
define("_ADDSECTION","Legg til en ny kategori");
define("_SECTIONNAME","Seksjonsnavn");
define("_SECTIONIMG","Seksjonsbilde");
define("_SECIMGEXAMPLE","(Vil v�re under /images/sections/ katalogen. Eksempel: opinion.gif)");
define("_ADDSECTIONBUT","Legg til seksjon");
define("_SELSECTION","Velg seksjon");
define("_SECTIONHAS","Dette avsnittet har");
define("_ARTICLESATTACH","Artikler lagt ved");
define("_40CHARSMAX","(40 tegn maks.)");
define("_EDITSECTION","Rediger seksjon");
define("_DELSECWARNING","Er du sikker p� du vil slette denne seksjonen");
define("_DELSECWARNING1","Dette vil slette ALLE dens artikler!");
define("_DELARTWARNING","Er du sikker p� du vil slette denne artikkelen?");
define("_DELSECTION","Slett seksjonen");
define("_DELARTICLE","Slett artiklen");

/*****************************************************/
/* Ephemerids Administration texts                   */
/*****************************************************/

define("_EPHEMADMIN","Administrer begivenheter");
define("_ADDEPHEM","Legg til ny begivenhet");
define("_EPHEMDESC","Beskriv begivenhet");
define("_EPHEMMAINT","Vedlikeholde begivenheter (Redigere/Slette):");
define("_EPHEMEDIT","Rediger begivenheter");

/*****************************************************/
/* Reviews Administration texts                      */
/*****************************************************/

define("_REVADMIN","Administrer anmeldelser");
define("_REVTITLE","Innledningstittel");
define("_REVDESC","Innledningstekst");
define("_REVWAITING","Anmeldelser som venter p� godkjenning");
define("_REVIMGINFO","Lagre dine 150*bilder i images/reviews");
define("_TEXT","Tekst");
define("_IMAGE","Bilde");
define("_NOREVIEW2ADD","Ingen anmeldelser � legge til");
define("_ADDREVIEW","Legg til anmeldelse");
define("_CLICK2ADDREVIEW","Klikk her for � skrive en anmeldelse");
define("_DELMODREVIEW","Slett/rediger en anmeldelse");
define("_MODREVINFO","Du kan enkelt slette/redigere anmeldelser ved � surfe innom <a href=\"reviews.php\">reviews.php</a> s� lenge du er innlogget som administrator.");

/*****************************************************/
/* File Manager Administration texts                 */
/*****************************************************/

define("_SIZE","St�rrelse");
define("_MODIFIED","Endret");
define("_PARENTDIR","Forrige katalog");
define("_CHANGEDIR","Endre jobb katalog til");
define("_MOVRENCP","Flytt, omd�p eller kopier");
define("_TOUCH","Trykk");
define("_AUDIO","Audio");
define("_WEBPROGRAM","Webprogram");
define("_APACHESET","Apache Webserver sikkerhetsinnstillinger");
define("_WEBPAGE","Webside");
define("_UNKOWNFT","Ukjent filtype");
define("_UPLOADFILE","Last opp fil");
define("_CREATEDIR","Opprett katalog");
define("_CREATEFILE","Opprett fil");
define("_HTMLTEMP","(html mal)");
define("_UPLOADED","Opplastet");
define("_LISTINGDIR","Lister katalogen");
define("_BROWSE","Bla igjennom");
define("_LISTDIR","Lister katalogen");
define("_CHANGED2ROOT","Endre til root katalog");
define("_DISPHP","Viser PHP environment");
define("_CHANGEDDIR","Endre katalog til");
define("_TOUCHED","Trykket");
define("_DELETED","Slettet");
define("_FMSURE2DEL","Er du sikker du �nsker � SLETTE");
define("_DESTFILEEXT","M�l fil eksisterer allerede");
define("_COPIED","Kopiert");
define("_MOVREN","Flyttet/omd�pt");
define("_MOVRENAMING","Flytter/endrer navn eller kopierer");
define("_COPY","Kopier");
define("_MOVREN2","Flytt/omd�p");
define("_EDITED","Redigert");
define("_EDITING","Redigerer");
define("_DISPLAYING","Viser");
define("_THEDIR","Katalogen");
define("_ALREADYEXT","eksisterer allerede.");
define("_DIRCREATED","Laget katalog");
define("_CREATED","Laget");
define("_CREATING","Lager");
define("_UNTITLED","Uten tittel");

/*****************************************************/
/* Downloads Administration texts                    */
/*****************************************************/

define("_DOWNLOADSINDB","Downloads i databasen v�r");
define("_DOWNLOADSWAITINGVAL","Downloads som venter p� validering");
define("_CLEANDOWNLOADSDB","Slett downloads stemmer");
define("_BROKENDOWNLOADSREP","Rapporterte d�de downloadlinker");
define("_DOWNLOADMODREQUEST","Download modifikasjonsforesp�rsler");
define("_ADDNEWDOWNLOAD","Legg til ny download");
define("_MODDOWNLOAD","Modifisier en download");
define("_WEBDOWNLOADSADMIN","Downloads administrasjon");
define("_DNOREPORTEDBROKEN","No reported broken downloads.");
define("_DUSERREPBROKEN","User Reported Broken Downloads");
define("_DIGNOREINFO","Ignore (Deletes all <b><i>requests</i></b> for a given download)");
define("_DDELETEINFO","Delete (Deletes <b><i>broken download</i></b> and <b><i>requests</i></b> for a given download)");
define("_DOWNLOAD","Download");
define("_DOWNLOADOWNER","Download eier");
define("_DUSERMODREQUEST","User Download Modification Requests");
define("_DDELCATWARNING","WARNING: Are you sure you want to delete this Category and ALL its Downloads?");
define("_DOWNLOADVALIDATION","Download validering");
define("_CHECKALLDOWNLOADS","Sjekk ALLE downloadlinker");
define("_VALIDATEDOWNLOADS","Valider downloads");
define("_NEWDOWNLOADADDED","Ny fil lagt til i databasen");
define("_YOURDOWNLOADAT","Din download i");
define("_DWEAPPROVED","We approved your download submission for our search engine.");

/*****************************************************/
/* Web Links Administration texts                    */
/*****************************************************/

define("_LINKSINDB","Linker i databasen v�r");
define("_LINKSWAITINGVAL","Linker som venter p� validering");
define("_SUBMITTER","Innsender");
define("_NONE","Ingen");
define("_VISIT","Bes�k");
define("_CLEANLINKSDB","Slett brukerstemmene");
define("_BROKENLINKSREP","D�de linker");
define("_LINKMODREQUEST","Forslag til endringer");
define("_ADDMAINCATEGORY","Legg til hovedkategori");
define("_ADDSUBCATEGORY","Legg til underkategori");
define("_IN","i");
define("_ADDNEWLINK","Legg til ny link");
define("_DESCRIPTION255","Beskrivelse: (maks 255 tegn)");
define("_MODCATEGORY","Rediger en kategori");
define("_MODLINK","Rediger en link");
define("_WEBLINKSADMIN","Administrer Weblinker");
define("_ADDEDITORIAL","Legg til notat");
define("_EDITORIALTITLE","Notat tittel");
define("_EDITORIALTEXT","Notat tekst");
define("_DATEWRITTEN","Dato skrevet");
define("_NOREPORTEDBROKEN","Ingen d�de linker rapportert.");
define("_USERREPBROKEN","D�de linker rapportert av brukere");
define("_IGNOREINFO","Ignorer (Sletter alle <b><i>foresp�rsler</i></b> for gitt link)");
define("_DELETEINFO","Slett (Sletter <b><i>d�de linker</i></b> og <b><i>foresp�rsler</i></b> for gitt link)");
define("_LINK","Link");
define("_LINKOWNER","Link eier");
define("_IGNORE","Ignorere");
define("_USERMODREQUEST","Linkmodifisering foresl�tt av brukere");
define("_ORIGINAL","Orginal");
define("_PROPOSED","Foresl�tt");
define("_NOMODREQUESTS","Det er ingen forslag til modifisering akkurat n�");
define("_SUBCATEGORY","Underkategori");
define("_OWNER","Eier");
define("_ACCEPT","Aksepter");
define("_DELCATWARNING","ADVARSEL: Er du sikker p� at du vil slette denne kategorien og alle linkene som h�rer til?");
define("_ERRORTHECATEGORY","FEIL: Kategorien");
define("_ALREADYEXIST","eksisterer allerede!");
define("_ERRORTHESUBCATEGORY","FEIL: Underkategorien");
define("_EDITORIALADDED","Notat lagt til databasen");
define("_EDITORIALMODIFIED","Notat redigert");
define("_EDITORIALREMOVED","Notat er fjernet fra databasen");
define("_LINKVALIDATION","Linkvalidering");
define("_CHECKALLLINKS","Sjekk ALLE linker");
define("_CHECKCATEGORIES","Sjekk kategoriene");
define("_INCLUDESUBCATEGORIES","(inkluder underkategori)");
define("_CHECKSUBCATEGORIES","Sjekk underkategorier");
define("_VALIDATELINKS","Sjekk linker");
define("_FAILED","Misslykket!");
define("_BEPATIENT","(vennligst ha t�lmodighet)");
define("_VALIDATINGCAT","Validerer kategori (og underkategorier)");
define("_VALIDATINGSUBCAT","Validerer underkategori");
define("_ERRORURLEXIST","FEIL: Denne URL'n eksisterer allerede i databasen!");
define("_ERRORNOTITLE","FEIL: Du m� skrive en TITTEL for URL'n!");
define("_ERRORNOURL","FEIL: Du m� skrive en URL for linken!");
define("_ERRORNODESCRIPTION","FEIL: Du m� skrive en BESKRIVELSE til URL'n din!");
define("_NEWLINKADDED","Ny link er lagt til i databasen");
define("_YOURLINKAT","Din link hos");
define("_WEAPPROVED","Vi godkjente linken du sendte inn til v�r database.");
define("_YOUCANBROWSEUS","Du kan browse v�r linkdatabase her:");
define("_THANKS4YOURSUBMISSION","Takk for ditt bidrag!");
define("_TEAM","Team.");

/*****************************************************/
/* Messages System texts                             */
/*****************************************************/

define("_MESSAGES","Meldinger");
define("_MESSAGESADMIN","Administrer meldinger");
define("_MESSAGETITLE","Tittel");
define("_MESSAGECONTENT","Innhold");
define("_EXPIRATION","G�r ut");
define("_VIEWPRIV","Hvem kan se dette?");
define("_MVADMIN","Bare Admin");
define("_MVUSERS","Bare brukere");
define("_MVANON","Bare anonyme");
define("_MVALL","Alle bes�kende");
define("_CHANGEDATE","Forandre startdato til idag?");
define("_IFYOUACTIVE","(Dersom du aktivere denne meldingen n�, s� vil startdatoen bli idag)");

/*****************************************************/
/* Addons System texts                               */
/*****************************************************/

define("_MAKELANG","Language File");
define("_GUESTBOOK","Guestbook");
define("_TOP10","Top 10");
define("_YOURACCOUNT","Your Account");
define("_HOME","Main Page");
define("_WELCOME","Welcome ");
define("_JAN","January");
define("_FEB","February");
define("_MAR","March");
define("_APR","April");
define("_MAY","May");
define("_JUN","June");
define("_JUL","July");
define("_AUG","August");
define("_SEP","September");
define("_OCT","October");
define("_NOV","November");
define("_DEC","December");
define("_GALLERY","Gallery");
define("_XSHOWUSER","Number of listed Members in <i>Members List</i> (per Page)");
define("_Glossary","Glossary");

/*****************************************************/
/* PHPBB Forum texts                                 */
/*****************************************************/

define("_FORUMMANAGER","Forum Manager");
define("_FORUMRANKS","Forum Ranks");
define("_FORUMCONFIG","Forum Configuration");
define("_NORANK","No Special Rank Assigned");
define("_NODBRANK","No Special Ranks in Database");
define("_USERLEVEL","User Level");
define("_RANK","Rank");
define("_FORUMS","Forums");

/*****************************************************/
/* From file : modules/All_Stories                   */
/*****************************************************/
define("_ALLSMAX","Maximum number of Stories to display ");
define("_ALLSTORIES1","All Stories");
define("_THEREIS","There are");
define("_ARTICLES","Articles");
define("_TOTAL","Total");
define("_ACTIONS","Actions");
define("_READ","Read");
define("_POSTED","Posted on");

/*****************************************************/
/* NEW MULTILING FIELDS BY CROCKET (webmasters.be)   */
/*****************************************************/

define("_ULANGUAGE","Language");
define("_COMPANY","Company");
define("_ADDRESS1","Address");
define("_ADDRESS2","Address");
define("_ZIP","Zipcode");
define("_CITY","City");
define("_COUNTRY","Country");
define("_TEL","Tel.");
define("_FAX","Fax");
define("_OPTIN","Newsletter");
define("_OPTINNEWSLETTER","Yes I will receive the newsletter");
define("_LANGUAGE","Language");

/**************************************************************************************************/
/* Regional Specific Date texts                                                                   */
/*                                                                                                */
/* A little help for date manipulation, from PHP manual on function strftime():                   */
/*                                                                                                */
/* %a - abbreviated weekday name according to the current locale                                  */
/* %A - full weekday name according to the current locale                                         */
/* %b - abbreviated month name according to the current locale                                    */
/* %B - full month name according to the current locale                                           */
/* %c - preferred date and time representation for the current locale                             */
/* %C - century number (the year divided by 100 and truncated to an integer, range 00 to 99)      */
/* %d - day of the month as a decimal number (range 01 to 31)                                     */
/* %D - same as %m/%d/%y                                                                          */
/* %e - day of the month as a decimal number, a single digit is preceded by a space               */
/*      (range ' 1' to '31')                                                                      */
/* %h - same as %b                                                                                */
/* %H - hour as a decimal number using a 24-hour clock (range 00 to 23)                           */
/* %I - hour as a decimal number using a 12-hour clock (range 01 to 12)                           */
/* %j - day of the year as a decimal number (range 001 to 366)                                    */
/* %m - month as a decimal number (range 01 to 12)                                                */
/* %M - minute as a decimal number                                                                */
/* %n - newline character                                                                         */
/* %p - either `am' or `pm' according to the given time value, or the corresponding strings for   */
/*      the current locale                                                                        */
/* %r - time in a.m. and p.m. notation                                                            */
/* %R - time in 24 hour notation                                                                  */
/* %S - second as a decimal number                                                                */
/* %t - tab character                                                                             */
/* %T - current time, equal to %H:%M:%S                                                           */
/* %u - weekday as a decimal number [1,7], with 1 representing Monday                             */
/* %U - week number of the current year as a decimal number, starting with the first Sunday as    */
/*      the first day of the first week                                                           */
/* %V - The ISO 8601:1988 week number of the current year as a decimal number, range 01 to 53,    */
/*      where week 1 is the first week that has at least 4 days in the current year, and with     */
/*      Monday as the first day of the week.                                                      */
/* %W - week number of the current year as a decimal number, starting with the first Monday as    */
/*      the first day of the first week                                                           */
/* %w - day of the week as a decimal, Sunday being 0                                              */
/* %x - preferred date representation for the current locale without the time                     */
/* %X - preferred time representation for the current locale without the date                     */
/* %y - year as a decimal number without a century (range 00 to 99)                               */
/* %Y - year as a decimal number including the century                                            */
/* %Z - time zone or name or abbreviation                                                         */
/* %% - a literal `%' character                                                                   */
/*                                                                                                */
/*                                                                                                */
/* Note: _DATESTRING is used for Articles and Comments Date                                       */
/*       _LINKSDATESTRING is used for Web Links creation Date                                     */
/*       _DATESTRING2 is used for Older Articles box on Home                                      */
/*                                                                                                */
/**************************************************************************************************/

define("_DATESTRING","%A, %B %d @ %T %Z");
define("_LINKSDATESTRING","%d-%b-%Y");
define("_DATESTRING2","%A, %B %d");

/*****************************************************/
/* Function to translate Datestrings                 */
/*****************************************************/

function translate($phrase) {
    switch($phrase) {
    case "xdatestring": $tmp = "%A, %B %d @ %T %Z"; break;
    case "linksdatestring": $tmp = "%d-%b-%Y"; break;
    case "xdatestring2": $tmp = "%A, %B %d"; break;
	default: 			$tmp = "$phrase"; break;
    }
    return $tmp;
}

?>
