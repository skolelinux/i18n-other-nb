#!/usr/bin/perl
#
# Tool to convert norwegian (and probably other) affix-files to the format required by MySpell
#
#
# Author: Marius Kotsbak <marius@kotsREMOVEbak.com>
# License: GPL (newest version)
#

$usage = "\nreads input on std.in, and outputs to std.out.\n
ispell2myspell.pl [type] < inputfile > outputfile\n
where type = 'PFX', 'SFX' etc.\n\n";

if ( not $type = shift ) {
  print $usage;
  exit;
}

@lines = <STDIN>;

foreach $line (@lines) {
  if ( $line =~ m/flag/i ) {
    if ( $num > 0 ) {   #  this isn't the first flag
      print "$type $flag $combine $num\t#$flagcomments\n$new";
    }

    $new = '';
    $num = 0;
    
    ($flag,$flagcomments) = split ( '#', $line );
    chop($flagcomments);  # remove \n
#    if ($flagcomments ) {
#      print '#'.$flagcomments;
#    }

    ($dummy,$flag) = split( ' ', $line );

    if ( $flag =~ m/\*/i ) {
      $combine = 'Y';
      $flag = substr( $flag, 1, 1 );
    }
    else {
      $combine = 'N';
    }
  }
  elsif ( $line =~ m/^\#/ ) { # outcommented lines
	  $new = $new.$line;
  }
  elsif ( $line =~ m/>/i ) { #normal line, not outcommented
    ($line, $comments) = split('#', $line);
    ($from, $to) = split('>',$line );

    
    $from = trim($from);
    $to   = trim($to);
    
    if ( $to =~ m/,/ ) { # found something to remove
      ($remove, $to) = split( ',', $to );
      $remove = substr( $remove, 1 );
    }
    else { #nothing
      $remove = '0';
    }
    
    $new = $new."$type $flag $remove\t$to\t$from \t# $comments";
    $num++;
  }
  else {
    print $line;
  }
}

sub trim() {
  $line = shift @_;
  $line =~ s/^\s+//;
  $line =~ s/\s+$//;

  return $line;
}



